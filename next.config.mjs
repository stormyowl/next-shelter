/** @type {import('next').NextConfig} */
const nextConfig = {
  trailingSlash: true,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "photos.skpb.org",
        port: "",
      },
    ],
  },
};

export default nextConfig;
