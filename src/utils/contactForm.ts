import { z } from "zod";

export const required_error = "To pole jest wymagane.";

export const contactFormSchema = z.object({
  name: z.string({ required_error }).min(1, { message: required_error }).trim(),
  email: z
    .string({ required_error })
    .min(1, { message: required_error })
    .email({ message: "To nie jest poprawny adres e-mail." })
    .trim(),
  subject: z.string({ required_error }).min(1, { message: required_error }).trim(),
  message: z.string({ required_error }).min(1, { message: required_error }).trim(),
  sendCopy: z.boolean({ required_error }),
});
