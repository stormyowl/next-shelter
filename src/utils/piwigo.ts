import axios from "axios";
import { getPlaiceholder } from "plaiceholder";

type PhotoDerivative = {
  url: string;
  width: number;
  height: number;
};

type PiwigoAlbumResponse = {
  stat: string;
  result: {
    paging: {
      page: number;
      per_page: number;
      count: number;
      total_count: string;
    };
    images: {
      is_favorite: boolean;
      id: number;
      width: number;
      height: number;
      hit: number;
      file: string;
      name: string;
      comment: null;
      date_creation: string;
      date_available: string;
      page_url: string;
      element_url: string;
      download_url: string;
      derivatives: {
        square: PhotoDerivative;
        thumb: PhotoDerivative;
        "2small": PhotoDerivative;
        xsmall: PhotoDerivative;
        small: PhotoDerivative;
        medium: PhotoDerivative;
        large: PhotoDerivative;
        xlarge: PhotoDerivative;
        xxlarge: PhotoDerivative;
      };
      categories: {
        id: number;
        url: string;
        page_url: string;
      }[];
    }[];
  };
};

export async function fetchPhotosFromPiwigoAlbum(albumId: number) {
  try {
    const {
      data: {
        result: {
          paging: { total_count, ...paging }, // eslint-disable-line camelcase
          images,
        },
      },
    } = await axios.get<PiwigoAlbumResponse>(process.env.PIWIGO_API_URL as string, {
      responseType: "json",
      params: {
        format: "json",
        method: "pwg.categories.getImages",
        cat_id: albumId,
      },
    });

    return Promise.all(
      images.map(
        async ({
          file: title,
          comment: caption,
          element_url: url,
          width,
          height,
          derivatives: { xsmall, small, medium, large, xlarge, xxlarge },
        }) => {
          // If the image is external (remote), we'd want
          // to fetch it first
          const imageRes = await fetch(url);

          // Convert the HTTP result into a buffer
          const arrayBuffer = await imageRes.arrayBuffer();
          const buffer = Buffer.from(arrayBuffer);

          // Calculate the base64 for the blur using the
          // same buffer
          const { base64 } = await getPlaiceholder(buffer);

          return {
            src: url,
            width,
            height,
            alt: caption || title,
            title,
            srcSet: [xsmall, small, medium, large, xlarge, xxlarge].map(({ url, width, height }) => ({
              src: url,
              width,
              height,
            })),
            blurDataURL: base64,
          };
        },
      ),
    );
  } catch {
    throw new Error("Failed to fetch gallery photos!");
  }
}
