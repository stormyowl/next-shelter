import { createRef } from "react";
import ReCAPTCHA from "react-google-recaptcha";

import { ContactFormContainer } from "@/components/contactForm/ContactFormContainer";
import { ContactLinks } from "@/components/contactLinks/ContactLinks";

const title = "Kontakt";

const Contact = () => {
  const reCaptcha = createRef<ReCAPTCHA>();

  return (
    <>
      <h1 className="title is-1">{title}</h1>
      <div className="columns">
        <div className="column">
          <ContactFormContainer reCaptcha={reCaptcha} />
        </div>
        <div className="column">
          <div className="content is-medium">
            <p>&ldquo;Chata na Zagroniu&rdquo; jest własnością Oddziału Międzyuczelnianego PTTK w Katowicach.</p>
            <p>
              Chatą opiekuje się <a href="https://skpb.org">Studenckie Koło Przewodników Beskidzkich w Katowicach</a>.
              Organem zarządzającym jest Rada Chatki, tel. 538 372 560.
            </p>
          </div>
          <ContactLinks />
        </div>
      </div>
    </>
  );
};

export default Contact;

export const metadata = { title };
