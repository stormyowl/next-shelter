import { GoogleAnalytics } from "@next/third-parties/google";
import type { Metadata } from "next";
import { Lato } from "next/font/google";
import { ToastContainer } from "react-toastify";

import { Layout } from "@/components/layout/Layout";

import "@/scss/style.scss";
import "react-toastify/dist/ReactToastify.css";

const lato = Lato({
  weight: ["400", "700"],
  style: ["normal", "italic"],
  subsets: ["latin"],
});

export const metadata: Metadata = {
  metadataBase: new URL(process.env.METADATA_BASE ?? ""),
  title: {
    template: "%s | Chata na Zagroniu",
    default: "Chata na Zagroniu",
  },
  description:
    "Chata na Zagroniu to chatka studencka, która znajduje się w Beskidzie Żywieckim, na grzbiecie poniżej Zapolanki, między Rajczą-Nickuliną, a Złatną, ok. 200 metrów od żółtego szlaku. Chatą opiekuje się Studenckie Koło Przewodników Beskidzkich w Katowicach.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="pl">
      <body className={lato.className}>
        <ToastContainer
          position="top-center"
          autoClose={3000}
          hideProgressBar
          newestOnTop
          closeOnClick={false}
          rtl={false}
          draggable
          pauseOnHover
        />
        <Layout>{children}</Layout>
      </body>
      <GoogleAnalytics gaId="G-0F9HCRREDF" />
    </html>
  );
}
