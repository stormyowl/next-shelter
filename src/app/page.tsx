import Image from "next/image";
import Link from "next/link";

import logo from "@/img/logo.png";

const Index = () => (
  <div className="content is-medium">
    <div className="columns is-multiline">
      <div className="column is-full-tablet is-one-third-desktop is-one-third-widescreen is-one-third-fullhd">
        <div style={{ maxWidth: 350, margin: "0 auto" }}>
          <Image src={logo} alt="Logo" placeholder="blur" />
        </div>
      </div>
      <div
        className="column is-full-tablet is-two-thirds-desktop is-two-thirds-widescreen is-two-thirds-fullhd"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <h1 className="is-hidden-touch">Chata na Zagroniu</h1>
        <p>
          <strong>Chata na Zagroniu</strong> to chatka studencka, która znajduje się w Beskidzie Żywieckim, na grzbiecie
          poniżej Zapolanki, między Rajczą-Nickuliną, a Złatną, ok. 200 metrów od żółtego szlaku. W terenie znajdują się
          stosowne drogowskazy w najważniejszych miejscach.
        </p>
        <p>
          Chatą opiekuje się <a href="https://skpb.org">Studenckie Koło Przewodników Beskidzkich w Katowicach</a>.
        </p>
      </div>

      <div className="column is-full-tablet is-half-desktop is-is-half-widescreen is-is-half-fullhd">
        <p>Zapraszamy w:</p>
        <ul>
          <li>
            <strong>sezonie letnim (lipiec - wrzesień):</strong> zgodnie z{" "}
            <Link href="/rezerwacje">kalendarzem chatkowań</Link>,
          </li>
          <li>
            <strong>pozostałą część roku:</strong> w weekendy podane w <Link href="/rezerwacje">kalendarzu</Link>{" "}
            <strong>
              tylko po wcześniejszej <Link href="/kontakt">rezerwacji</Link>
            </strong>
            .
          </li>
        </ul>
        <p>Oferujemy:</p>
        <ul>
          <li>20 miejsc noclegowych w warunkach turystycznych,</li>
          <li>miejsca na nocleg w namiotach własnych,</li>
          <li>bezpłatny wrzątek lub herbatę,</li>
          <li>do dyspozycji wyposażoną kuchnię i salon,</li>
          <li>umywalnię w pomieszczeniu gospodarczym,</li>
          <li>miejsce na ognisko przed chatą,</li>
          <li>kominek w salonie,</li>
          <li>w sezonie letnim prysznic na zewnątrz,</li>
          <li>Agricolę, Sabotażystę i inne gry planszowe.</li>
        </ul>
      </div>

      <div className="column is-full-tablet is-half-desktop is-is-half-widescreen is-is-half-fullhd">
        <p>
          Koszt noclegu w chacie to <strong>25 zł</strong>, w namiocie własnym <strong>10 zł</strong>.
        </p>
        <p>
          Respektujemy rabaty PTTK, ponadto oferujemy cenę specjalną <strong>15 zł</strong> dla członków i kursantów
          Studenckiego Koła Przewodników Beskidzkich w Katowicach oraz „trójkątnych” przewodników, a także dla
          uczestników przejść przewodnickich innych studenckich kół przewodnickich. Pełny cennik dostępny jest{" "}
          <a href="/files/cennik-2024.pdf">tutaj</a>.
        </p>
        <p>
          W chacie i jej bliskiej okolicy obowiązuje <strong>całkowity zakaz</strong> nadużywania{" "}
          <strong>alkoholu</strong>.
        </p>
        <p>
          Wybierając się do chaty na nocleg, koniecznie pamiętaj o <strong>zmiennym obuwiu</strong>!
        </p>
      </div>
    </div>
  </div>
);

export const metadata = {
  title: "O chacie | Chata na Zagroniu",
};

export default Index;
