import { Gallery as PhotoGallery } from "@/components/gallery/Gallery";
import { fetchPhotosFromPiwigoAlbum } from "@/utils/piwigo";

const title = "Galeria";

const Gallery = async () => {
  const photos = await fetchPhotosFromPiwigoAlbum(1);

  return (
    <>
      <h1 className="title is-1">{title}</h1>
      <PhotoGallery photos={photos} />
    </>
  );
};

export default Gallery;

export const metadata = { title };
