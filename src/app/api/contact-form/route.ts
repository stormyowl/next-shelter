import axios from "axios";
import { z } from "zod";

import { contactFormSchema, required_error } from "@/utils/contactForm";

const schema = contactFormSchema.extend({ token: z.string({ required_error }).min(1, { message: required_error }) });

export async function POST(req: Request) {
  const body = await req.json().catch(() => {});
  const result = schema.safeParse(body || {});

  if (result.success) {
    const { name, email, subject, message, sendCopy, token } = result.data;
    try {
      const { success } = await axios
        .get("https://www.google.com/recaptcha/api/siteverify", {
          params: {
            secret: process.env.RECAPTCHA_SECRET_KEY_V2,
            response: token,
          },
        })
        .then(({ data }) => data);

      if (!success) {
        return Response.json(
          { message: "Czy na pewno nie jesteś robotem? Jeśli nie to może wyślesz wiadomość e-mail?" },
          { status: 401 },
        );
      }
    } catch (error) {
      console.log(error);
      return Response.json({ message: "Nie udało się sprawdzić rozwiązania reCAPTCHY." }, { status: 503 });
    }

    const from = JSON.parse(process.env.SENDGRID_FROM as string);
    const to = JSON.parse(process.env.SENDGRID_TO as string);
    const bcc = JSON.parse(process.env.SENDGRID_BCC as string);
    const replyTo = { name, email };

    const payload = {
      personalizations: [
        {
          to: [to],
          ...(sendCopy && { cc: [replyTo] }),
          bcc: [bcc],
          subject,
        },
      ],
      from,
      reply_to: replyTo,
      content: [{ type: "text/plain", value: message }],
    };

    try {
      const headers = { Authorization: `Bearer ${process.env.SENDGRID_API_KEY}` };
      await axios.post("https://api.sendgrid.com/v3/mail/send", payload, {
        headers,
      });

      return Response.json({ message: "Twoja wiadomość została wysłana." }, { status: 200 });
    } catch (error) {
      console.log("SendGrid error:", {
        // @ts-ignore
        status: error.response.status,
        // @ts-ignore
        data: error.response.data,
      });
      // @ts-ignore
      return Response.json({ message: error.response }, { status: 503 });
    }
  } else {
    const errors = result.error.issues.reduce((acc, { path, message }) => ({ ...acc, [path.join(".")]: message }), {});
    return Response.json({ errors }, { status: 400 });
  }
}
