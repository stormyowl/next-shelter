import { Title } from "@/components/title/Title";

const title = "Regulaminy";

const Regulaminy = () => (
  <>
    <Title title={title} />
    <div className="content is-medium">
      <div className="columns">
        <div className="column">
          <h2>Regulamin Chaty na Zagroniu</h2>
          <p>Chata na Zagroniu jest własnością Oddziału Międzyuczelnianego PTTK w Katowicach.</p>
          <p>
            Chatą opiekuje się Studenckie Koło Przewodników Beskidzkich w Katowicach. Organem zarządzającym jest Rada
            Chaty.
          </p>
          <ol>
            <li>Chata otwarta jest dla każdego Turysty przestrzegającego niniejszego regulaminu.</li>
            <li>
              Każdy Gość, pozostający w chatce, zobowiązany jest do zameldowania się u chatkowego i respektowania jego
              zaleceń. Nocleg podlega opłacie zgodnej z cennikiem.
            </li>
            <li>
              Chata nie prowadzi usług turystycznych ani gastronomicznych. Na terenie posesji i w jej obrębie każdy
              przebywa dobrowolnie i na własną odpowiedzialność.
            </li>
            <li>
              W chacie należy poruszać się w obuwiu zmiennym. Każdy Gość zobowiązany jest do dbania o czystość i
              porządek w chacie oraz jej otoczeniu.
            </li>
            <li>
              Posiłki sporządza się wyłącznie w kuchni. Każdy Gość ma prawo do korzystania z infrastruktury kuchennej.
            </li>
            <li>Każdy zobowiązany jest do segregowania odpadów.</li>
            <li>
              Odpowiedzialność za zniszczenia ponosi sprawca, uiszczając odpowiednio wysoką opłatę, której wysokość
              odpowiada kosztom naprawy. Wysokość opłaty za usunięcie napisów ze ścian wynosi 250 zł.
            </li>
            <li>
              Turysta zobowiązany jest do pilnowania rzeczy osobistych. Przedmioty pozostawione w chacie są cyklicznie
              utylizowane.
            </li>
            <li>
              Osoby nieletnie mogą przebywać na terenie chaty jedynie pod opieką dorosłego opiekuna, który ponosi za nie
              odpowiedzialność.
            </li>
            <li>
              W chacie obowiązuje całkowity zakaz używania otwartego ognia. Palenie wyrobów tytoniowych i e-papierosów
              dozwolone jest tylko w wyznaczonym miejscu.
            </li>
            <li>
              Na terenie chatki obowiązuje całkowity zakaz spożywania środków odurzających oraz nadużywania alkoholu.
            </li>
            <li>
              Obecność zwierzęcia na terenie chatki jest dozwolona tylko w obrębie werandy. Za zachowanie zwierzęcia
              oraz szkody i zanieczyszczenia spowodowane jego obecnością, odpowiada osoba, która zwierzę przyprowadziła.
            </li>
            <li>
              Goście nie przestrzegający regulaminu lub zachowujący się w sposób zakłócający spokój i narażający na
              niebezpieczeństwo innych będą wydalani z chaty bez prawa do zwrotu uiszczonej opłaty noclegowej.
            </li>
            <li>
              O sprawach nieuregulowanych niniejszymi przepisami decyduje chatkowy. Wszelkie skargi dotyczące obsługi i
              funkcjonowania chaty należy zgłaszać na bieżąco Radzie Chaty telefonicznie lub w formie pisemnej na adres:
              Studenckie Koło Przewodników Beskidzkich przy OM PTTK w Katowicach, ul. Warszawska 6/302, 40-006 Katowice.
            </li>
          </ol>
        </div>
        <div className="column">
          <h2>Regulamin chatkowania</h2>
          <ol>
            <li>
              Chatkowanie polega na opiece nad obiektem i obsłudze turystów w Chacie na Zagroniu. Do obowiązków
              chatkowego należą między innymi:
            </li>
            <ul>
              <li>obecność w chacie w trakcie chatkowania,</li>
              <li>dbanie o porządek, ciepło i dobry klimat w chacie,</li>
              <li>kulturalna i profesjonalna obsługa turystów,</li>
              <li>dbanie o dobry wizerunek chaty i SKPB Katowice.</li>
            </ul>
            <li>
              Chatkować mogą jedna lub dwie osoby. Osoby te, rozumiane jako obsługa (a także ich niepełnoletnie dzieci),
              nie płacą za noclegi.
            </li>
            <li>
              Chatkować mogą przewodnicy, sympatycy i kursanci SKPB Katowice, oraz osoby związane z chatkami, bazami
              namiotowymi i kołami przewodnickimi oraz znane w środowisku turystycznym.
            </li>
            <li>
              Aby sprawować opiekę nad obiektem wystarczy jedna osoba z powyższego grona, wówczas to na niej spoczywa
              odpowiedzialność za funkcjonowanie chaty. Chatkować można np.: z dziewczyną, bratem, ciotką, dziadkiem,
              kolegą. Oboje traktowani są jako chatkowi.
            </li>
            <li>
              W przypadku, gdy chata nie jest zamknięta pomiędzy dyżurami chatkowi zobowiązani są do porozumienia się ze
              swoimi poprzednikami i następcami co do planów i pory wymiany obsady w chacie.
            </li>
            <li>
              Chata nie powinna pozostawać pusta i zamknięta w czasie chatkowania, poza przypadkami uzgodnionymi z Radą
              Chaty.
            </li>
            <li>
              Zapisy na chatkowanie prowadzi wyznaczona osoba z Rady Chaty (informacja na stronie internetowej w
              zakładce „Rezerwacje”). Termin chatkowania można uznać za zarezerwowany dopiero po potwierdzeniu przez tę
              osobę.
            </li>
          </ol>
        </div>
      </div>
    </div>
  </>
);

export const metadata = { title };

export default Regulaminy;
