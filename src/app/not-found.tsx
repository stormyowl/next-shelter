import Link from "next/link";

import { Title } from "@/components/title/Title";

const title = "Strony nie znaleziono";

const NotFound = () => (
  <>
    <Title title={title} />
    <div className="content is-medium">
      <p>
        Strona o podanym adresie nie istnieje lub została usunięta. Kliknij <Link href="/">tutaj</Link> aby
        przejść&nbsp;do strony głównej.
      </p>
    </div>
  </>
);

export const metadata = { title };

export default NotFound;
