import Image from "next/image";

import map from "@/img/mapa.jpg";
import { Title } from "@/components/title/Title";

const title = "Dojazd";

const Dojazd = () => (
  <>
    <Title title="Dojazd i propozycje wycieczek" />
    <div className="content is-medium">
      <Image src={map} alt="Mapa okolic Zagronia" placeholder="blur" />
      <p>Do Chaty na Zagroniu możecie dotrzeć samochodem, busem oraz pociągiem.</p>
      <h2>Samochód</h2>
      <p>
        Samochodem osobowym najbliżej chaty dojedziecie do kaplicy pw. Miłosierdzia Bożego w Rajczy Nickulinie, gdzie na
        parkingu możecie zostawić samochód i udać się piechotą do chaty{" "}
        <a href="https://mapy.cz/turisticka?planovani-trasy&x=19.1455959&y=49.5021969&z=16&rc=9sWrSxUfq7iCpevk&rs=coor&rs=osm&ri=&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%5D">
          żółtym szlakiem
        </a>{" "}
        skręcając w prawo z głównej drogi około 30 m za kaplicą.
      </p>
      <p>Możecie również skorzystać z innych wariantów dojścia:</p>
      <ul>
        <li>
          <strong>z Rajczy Centrum:</strong>{" "}
          <a href="https://en.mapy.cz/turisticka?planovani-trasy&x=19.1220729&y=49.5035347&z=14&rc=9sREYxUgsd9sZuH39v&rs=coor&rs=osm&ri=&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%5D">
            żółtym szlakiem
          </a>{" "}
          ze stacji kolejowej PKP Rajcza Centrum, w kierunku Hali Lipowskiej - 1:50 h,
        </li>
        <li>
          <strong>z Rajczy:</strong>{" "}
          <a href="https://en.mapy.cz/turisticka?planovani-trasy&x=19.1266897&y=49.5012961&z=14&rc=9sRkkxUfuH5UU3P-joDez1h..5dS&rs=coor&rs=coor&rs=coor&rs=osm&ri=&ri=&ri=&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%5D">
            Wilczym Szlakiem
          </a>{" "}
          chatkowym rozpoczynającym się za kościołem przez Compel, Wilczy Groń i Kiczorę - 2 h,
        </li>
        <li>
          <strong>z Ujsół:</strong>{" "}
          <a href="https://mapy.cz/turisticka?planovani-trasy&x=19.1509067&y=49.4949394&z=14&rc=9sWwsxU1GD5yPlfV&rs=osm&rs=osm&ri=39647035&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%5D">
            czarnym szlakiem
          </a>
          , później ok. 200 m żółtym w stronę Nickuliny - 1:15 h,
        </li>
        <li>
          <strong>ze Złatnej:</strong>{" "}
          <a href="https://mapy.cz/turisticka?planovani-trasy&x=19.1696499&y=49.5027833&z=14&rc=9sZuHxUemZizG5TXTscYk&rs=osm&rs=coor&rs=osm&ri=6538016&ri=&ri=108674422&mrp=%7B%22c%22%3A132%7D&xc=%5B%22POL%22%2C%22SVK%22%5D">
            drogą
          </a>{" "}
          na przysiółek Zapolanka, a następnie żółtym szlakiem do chaty - 2 h.
        </li>
      </ul>
      <p>
        Ostatnie 200 m do Chaty trzeba przejść drogą odchodzącą od szlaku żółtego nieco poniżej grzbietu - za
        drogowskazem.
      </p>
      <h2>Komunikacja publiczna</h2>
      <p>
        Najprostszym sposobem dotarcia do chaty transportem zbiorowym jest podróż pociągiem na trasie Katowice - Tychy -
        Bielsko-Biała - Żywiec - Rajcza (<a href="https://www.pkp.pl/">https://www.pkp.pl/</a>).
      </p>
      <p>
        Z Rajczy zostaje wam już tylko dojście do chaty{" "}
        <a href="https://mapy.cz/turisticka?planovani-trasy&x=19.1292424&y=49.5085870&z=14&rc=9sRTPxUft15mp3RGjoDez1h..5dS&rs=osm&rs=coor&rs=coor&rs=osm&ri=27397885&ri=&ri=&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%5D">
          Wilczym Szlakiem
        </a>{" "}
        lub{" "}
        <a href="https://mapy.cz/turisticka?planovani-trasy&x=19.1247976&y=49.5071089&z=14&rc=9sT6ZxUjgcmfibEw&rs=osm&rs=osm&ri=126945303&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%5D">
          żóltym szlakiem
        </a>{" "}
        PTTK.
      </p>
      <h2>Możliwe wycieczki</h2>
      <ul>
        <li>
          kilkugodzinne:{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.5256690,19.1817160#49.51005/19.17998/14">
            Redykalny Wierch
          </a>
          ,{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.5364040,19.2346060#49.52306/19.19655/13">
            Rysianka
          </a>
          ,{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.5422370,19.1692600#49.51036/19.18831/13">
            Hala Boracza
          </a>
          ,
        </li>
        <li>
          całodniowe:{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.5608430,19.2418890#49.54568/19.27088/13">
            Romanka
          </a>
          ,{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.5279550,19.3168350#49.52028/19.23603/13">
            Pilsko
          </a>
          ,{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.4189460,19.0973790#49.45739/19.18797/13">
            Rycerzowa
          </a>
          ,{" "}
          <a href="https://mapy.cz/turisticka?planovani-trasy&x=19.1750747&y=49.4899345&z=13&rc=9sZuHxUemZL5Nei.Z5Ssmp2xUYtq1Q7ljpeRFxU8izdoeifkgoRxUemZ&rs=osm&rs=osm&rs=coor&rs=coor&rs=osm&rs=osm&rs=osm&rs=osm&ri=6538016&ri=108674422&ri=&ri=&ri=108674422&ri=94294413&ri=13981&ri=6538016&mrp=%7B%22c%22%3A132%7D&xc=%5B%22POL%22%2C%22SVK%22%5D">
            Krawców Wierch
          </a>
          ,{" "}
          <a href="https://mapa-turystyczna.pl/route?q=49.4997544,19.1522384;49.4552890,19.1045380#49.48438/19.20582/13">
            Muńcuł
          </a>
          .
        </li>
      </ul>
    </div>
  </>
);

export const metadata = { title };

export default Dojazd;
