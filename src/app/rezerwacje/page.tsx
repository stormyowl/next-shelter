import Link from "next/link";

import { Title } from "@/components/title/Title";

const title = "Rezerwacje";

const Rezerwacje = () => (
  <>
    <Title title={title} />
    <div className="content is-medium">
      {" "}
      <p>
        {" "}
        Rezerwacje przyjmuje Jolanta Mazurek pod tel. 538 372 560, pod adresem e-mail{" "}
        <a href="mailto:zagron@skpb.org">zagron@skpb.org</a> lub przez <Link href="/kontakt">formularz kontaktowy</Link>{" "}
        .{" "}
      </p>{" "}
      <div className="columns is-multiline">
        {" "}
        <div className="column is-full-tablet is-full-desktop is-two-thirds-widescreen is-two-thirds-fullhd is-flex">
          {" "}
          <iframe
            src="https://www.google.com/calendar/embed?showTitle=0&amp;showNav=1&amp;showDate=1&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;hl=pl&amp;bgcolor=%23FFFFFF&amp;src=l5u811nje3ijtl3g4j73l407e8%40group.calendar.google.com&amp;color=%23182C57&amp;ctz=Europe%2FWarsaw"
            title="calendar"
            className="calendar"
          ></iframe>{" "}
        </div>{" "}
        <div className="column is-full-tablet is-full-desktop is-one-third-widescreen is-one-third-fullhd">
          {" "}
          <h2>Legenda</h2>{" "}
          <p>
            {" "}
            <strong>Chatkowanie</strong> - w chacie znajduje się obsługa, a więc możesz bez wahania zaplanować trasę z
            noclegiem w naszej chacie. Na pewno zostaniesz przywitany ciepłą herbatką i uśmiechem.{" "}
          </p>{" "}
          <p>
            {" "}
            <strong>Rezerwacja</strong> - spodziewamy się osób zapowiedzianych wcześniej, zwykle większej grupy. Jeśli
            jednak planujesz u nas nocleg z jakąś grupą, weź pod uwagę, że w obecnej sytuacji możemy udzielić noclegu 40
            osobom.{" "}
          </p>{" "}
        </div>{" "}
      </div>{" "}
      <p className="has-text-justified">
        W przypadku{" "}
        <b>
          rezerwacji noclegu dla grupy 10 i więcej osób konieczne jest wpłacenie na konto OM PTTK bezzwrotnego zadatku
        </b>{" "}
        wynoszącego 50% kwoty noclegu (tj. 12,5 zł/os./noc) w ciągu 3 dni od daty zgłoszenia rezerwacji. Rezerwacja
        zostaje potwierdzona dopiero po wpłacie zadatku, a{" "}
        <b>kwota zadatku nie ulega zwrotowi w przypadku rezygnacji z rezerwacji.</b> Dane do wpłaty zadatku można
        znaleźć poniżej.
      </p>
      <p>
        Polskie Towarzystwo Turystyczno-Krajoznawcze Oddział Międzyuczelniany w Katowicach
        <br />
        ul. Warszawska 6/302, 40-006 Katowice
        <br />
        <b>NIP:</b> 6340006217
        <br />
        <b>Nr konta bankowego:</b> 57 1020 2313 0000 3902 1142 6188 PKO BP
      </p>
      <p>
        Zadatek nie jest wymagany przy oficjalnych wydarzeniach oraz wyjazdach szkoleniowych SKPB Katowice. Obowiązuje
        za to w przypadku prywatnych imprez członków Koła.
      </p>
    </div>
  </>
);

export const metadata = { title };

export default Rezerwacje;
