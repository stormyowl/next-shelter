import logo from "@/img/logo.png";
import logoPTTK from "@/img/logo-pttk.png";
import logoSKPB from "@/img/logo-skpb.png";

import style from "./ContactLinks.module.scss";
import { ContactLink } from "./contactLink/ContactLink";

export const ContactLinks = () => (
  <div className={style.contactLinks}>
    <ContactLink href="/" image={logo} alt="Logo Chaty na Zagroniu" />
    <ContactLink href="https://skpb.org/" image={logoSKPB} alt="Logo SKPB" isExternal />
    <ContactLink href="https://pttk.pl/" image={logoPTTK} alt="Logo PTTK" isExternal />
  </div>
);
