import { StaticImageData } from "next/image";

export type ContactLinkProps = {
  href: string;
  image: StaticImageData;
  alt: string;
  isExternal?: boolean;
};
