import Image, { StaticImageData } from "next/image";
import Link from "next/link";

import style from "./ContactLink.module.scss";
import { ContactLinkProps } from "./ContactLink.types";

const wrapperStyle = ({ width, height }: StaticImageData) => ({
  flexBasis: (width / height) * 120,
  flexGrow: 0,
  flexShrink: 1,
  aspectRatio: width / height,
});

export const ContactLink = ({ href, image, alt, isExternal }: ContactLinkProps) => {
  const imageElement = <Image src={image} fill alt={alt} />;

  return (
    <div style={wrapperStyle(image)}>
      {isExternal ? (
        <a href={href} className={style.link}>
          {imageElement}
        </a>
      ) : (
        <Link href={href} className={style.link}>
          {imageElement}
        </Link>
      )}
    </div>
  );
};
