"use client";
import { useState } from "react";
import PhotoAlbum, { Photo } from "react-photo-album";
import Lightbox from "yet-another-react-lightbox";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import "react-photo-album/rows.css";
import "yet-another-react-lightbox/styles.css";
import "yet-another-react-lightbox/plugins/thumbnails.css";

import { GalleryProps } from "./Gallery.types";
import { renderNextImage } from "./Gallery.utils";

export const Gallery = ({ photos }: GalleryProps) => {
  const [index, setIndex] = useState(-1);

  return (
    <>
      <PhotoAlbum<Photo & { blurDataURL: string }>
        layout="rows"
        photos={photos}
        render={{
          image: renderNextImage,
        }}
        // Properties below are set based on Bulma's container widths.
        defaultContainerWidth={1344}
        sizes={{
          size: "1344px",
          sizes: [
            { viewport: "(max-width: 1023px)", size: "calc(100vw - 48px)" },
            { viewport: "(max-width: 1215px)", size: "960px" },
            { viewport: "(max-width: 1407px)", size: "1152px" },
          ],
        }}
        spacing={4}
        onClick={({ index }) => setIndex(index)}
      />
      <Lightbox
        slides={photos}
        open={index >= 0}
        index={index}
        close={() => setIndex(-1)}
        // enable optional lightbox plugins
        plugins={[Fullscreen, Slideshow, Thumbnails, Zoom]}
      />
    </>
  );
};
