import { Photo } from "react-photo-album";

type PhotoProp = Photo & { blurDataURL: string };

export type GalleryProps = {
  photos: PhotoProp[];
};
