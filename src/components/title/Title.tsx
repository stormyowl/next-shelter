import { TitleProps } from "./Title.types";

export const Title = ({ title }: TitleProps) => <h1 className="title is-1">{title}</h1>;
