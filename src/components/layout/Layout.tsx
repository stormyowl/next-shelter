import { ReactNode } from "react";

import { Header } from "@/components/header/Header";
import { NavbarContainer } from "@/components/navbar/NavbarContainer";

import style from "./Layout.module.scss";

const items = [
  { href: "/", label: "O chacie" },
  { href: "/dojazd/", label: "Dojazd" },
  { href: "/galeria/", label: "Galeria" },
  { href: "/regulaminy/", label: "Regulaminy" },
  { href: "/rezerwacje/", label: "Rezerwacje" },
  { href: "/kontakt/", label: "Kontakt" },
];

export const Layout = ({ children }: { children: ReactNode }) => (
  <div className={style.wrapper}>
    <NavbarContainer items={items} />
    <Header />
    <div className={style.content}>
      <div className="section">
        <div className="container">{children}</div>
      </div>
    </div>
    <footer className={style.footer}>
      <div className="container">&#169; 2024 Chata na Zagroniu. Wszystkie prawa zastrzeżone.</div>
    </footer>
  </div>
);
