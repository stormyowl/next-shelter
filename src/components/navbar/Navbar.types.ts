import { Dispatch, SetStateAction } from "react";

type NavbarItem = {
  href: string;
  label: string;
};

export type NavbarContainerProps = {
  items: NavbarItem[];
};

export type NavbarProps = NavbarContainerProps & {
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
};
