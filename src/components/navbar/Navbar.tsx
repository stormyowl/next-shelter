import Image from "next/image";
import Link from "next/link";

import { FacebookButton } from "@/components/facebookButton/FacebookButton";
import brand from "@/img/brand.png";

import "./Navbar.scss";
import { NavbarProps } from "./Navbar.types";
import clsx from "clsx";

export const Navbar = ({ isOpen, setIsOpen, items }: NavbarProps) => (
  <nav className="navbar is-sticky" role="navigation">
    <div className="container">
      <div className="navbar-brand">
        <Link href="/" className="navbar-item" style={{ padding: "0 0.75rem" }}>
          <Image src={brand} width={49} height={36} alt="Logo Chaty na Zagroniu" />
        </Link>
        <div
          className={clsx("navbar-burger", isOpen && "is-active")}
          role="button"
          onClick={() => setIsOpen((isOpen) => !isOpen)}
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </div>
      </div>
      <div className={clsx("navbar-menu", isOpen && "is-active")}>
        <div className="navbar-start">
          {items.map(({ href, label }, index) => (
            <Link href={href} key={index} className="navbar-item" onClick={() => setIsOpen(false)}>
              {label}
            </Link>
          ))}
        </div>
      </div>
      <div className="navbar-start">
        <div className="navbar-item">
          <div className="buttons">
            <FacebookButton href="https://www.facebook.com/chata.na.zagroniu" />
          </div>
        </div>
      </div>
    </div>
  </nav>
);
