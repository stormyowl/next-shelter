"use client";
import { useState } from "react";

import { Navbar } from "./Navbar";
import { NavbarContainerProps } from "./Navbar.types";

export const NavbarContainer = (props: NavbarContainerProps) => {
  const [isOpen, setIsOpen] = useState(false);

  return <Navbar isOpen={isOpen} setIsOpen={setIsOpen} {...props} />;
};
