import { BaseSyntheticEvent } from "react";
import { UseFormReturn } from "react-hook-form";
import { z } from "zod";

import { contactFormSchema as formSchema } from "@/utils/contactForm";

export type FormValues = z.infer<typeof formSchema>;

export type ContactFormProps = {
  form: UseFormReturn<FormValues>;
  onSubmit: (e?: BaseSyntheticEvent<object, any, any>) => Promise<void>;
};
