"use client";
import { zodResolver } from "@hookform/resolvers/zod";
import axios from "axios";
import { RefObject } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

import { contactFormSchema as formSchema } from "@/utils/contactForm";

import { ContactForm } from "./ContactForm";
import { defaultValues } from "./ContactForm.utils";
import { FormValues } from "./ContactForm.types";

export const ContactFormContainer = ({ reCaptcha }: { reCaptcha: RefObject<ReCAPTCHA> }) => {
  const form = useForm<FormValues>({
    resolver: zodResolver(formSchema),
    mode: "onBlur",
    reValidateMode: "onChange",
    defaultValues,
  });

  const onSubmit = async (values: FormValues) => {
    const token = await reCaptcha.current?.executeAsync();

    try {
      const body = { ...values, token };
      const {
        data: { message },
      } = await axios.post("/api/contact-form/", body);
      form.reset(defaultValues);
      toast.success(message);
    } catch (error) {
      const message =
        // @ts-ignore
        error?.response?.data?.message || "Nie udało się wysłać wiadomości! Może wyślesz wiadomość e-mail?";
      toast.error(message);
    }
  };

  return (
    <>
      <ReCAPTCHA sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY_V2 as string} size="invisible" ref={reCaptcha} />
      <ContactForm form={form} onSubmit={form.handleSubmit(onSubmit)} />
    </>
  );
};
