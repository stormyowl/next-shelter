export const options: string[] = ["Rezerwacja", "Chatkowanie", "Opinia", "Inny"];

export const defaultValues = {
  name: "",
  email: "",
  subject: options[0],
  message: "",
  sendCopy: true,
};
