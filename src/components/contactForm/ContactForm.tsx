import clsx from "clsx";
import { ReactNode } from "react";
import { Controller, FieldError } from "react-hook-form";

import { ContactFormProps } from "./ContactForm.types";
import { options } from "./ContactForm.utils";
import "./ContactForm.scss";

type FieldProps = { name: string; label: string; error?: FieldError; children: ReactNode };

const Field = ({ name, label, error, children }: FieldProps) => (
  <div className="field is-floating-label">
    <label htmlFor={name} className="label">
      {label}
    </label>
    <div className="control">{children}</div>
    {!!error && error.message && <p className="help is-danger">{error.message}</p>}
  </div>
);

export const ContactForm = ({
  form: {
    control,
    formState: { isSubmitting },
  },
  onSubmit,
}: ContactFormProps) => (
  <form onSubmit={onSubmit} noValidate>
    <Controller
      name="name"
      control={control}
      render={({ field, fieldState: { error } }) => (
        <Field name={field.name} label="Imię i nazwisko" error={error}>
          <input {...field} className={clsx("input", error && "is-danger")} />
        </Field>
      )}
    />
    <Controller
      name="email"
      control={control}
      render={({ field, fieldState: { error } }) => (
        <Field name={field.name} label="E-mail" error={error}>
          <input {...field} type="email" className={clsx("input", error && "is-danger")} />
        </Field>
      )}
    />
    <Controller
      name="subject"
      control={control}
      render={({ field, fieldState: { error } }) => (
        <Field name={field.name} label="Temat" error={error}>
          <div className="select is-fullwidth">
            <select {...field}>
              {options.map((option, index) => (
                <option value={option} key={index}>
                  {option}
                </option>
              ))}
            </select>
            {!!error && error.message && <p className="help is-danger">{error.message}</p>}
          </div>
        </Field>
      )}
    />
    <Controller
      name="message"
      control={control}
      render={({ field, fieldState: { error } }) => (
        <Field name={field.name} label="Wiadomość" error={error}>
          <textarea {...field} rows={4} className={clsx("textarea", "has-fixed-size", error && "is-danger")} />
        </Field>
      )}
    />
    <div className="field">
      <div className="control">
        <label className="checkbox">
          <Controller
            name="sendCopy"
            control={control}
            render={({ field: { name, value, onChange } }) => (
              <input type="checkbox" name={name} value={value.toString()} checked={!!value} onChange={onChange} />
            )}
          />
        </label>{" "}
        Wyślij do mnie kopię wiadomości
      </div>
    </div>
    <button
      type="submit"
      className={clsx("button", "is-link", "is-fullwidth", {
        "is-loading": isSubmitting,
      })}
    >
      Wyślij
    </button>
  </form>
);
