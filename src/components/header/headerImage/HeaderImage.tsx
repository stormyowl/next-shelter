import Image, { StaticImageData } from "next/image";

import style from "./HeaderImage.module.scss";
import { CSSProperties } from "react";

import { HeaderImageProps } from "./HeaderImage.types";

const getWrapperProps = ({ width, height }: StaticImageData): CSSProperties => ({
  width: "100%",
  aspectRatio: width / height,
  position: "relative",
});

export const HeaderImage = ({ image, alt, placeholder, className }: HeaderImageProps) => (
  <div style={getWrapperProps(image)} className={className}>
    <Image src={image} fill alt={alt} placeholder={placeholder} />
  </div>
);
