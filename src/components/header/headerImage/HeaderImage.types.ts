import { PlaceholderValue } from "next/dist/shared/lib/get-img-props";
import { StaticImageData } from "next/image";

export type HeaderImageProps = {
  image: StaticImageData;
  alt: string;
  placeholder?: PlaceholderValue;
  className?: string;
};
